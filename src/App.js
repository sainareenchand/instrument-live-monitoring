import LiveMonitoring from "./components/LiveMonitoring";
import LiveMonitoringMapBox from "./components/LiveMonitoringMapBox";
import { getInstrumentData, listInstrumentData } from "./graphql/queries";
import { API, graphqlOperation } from 'aws-amplify';
import { useEffect } from "react";
function App() {

  useEffect(() => {
    getAllInstruments();
  }, []);


  return (
    <div className="App">
      {/* <LiveMonitoring /> */}
      {/* <LiveMonitoringMapBox></LiveMonitoringMapBox> */}
    </div>
  );
}

const getAllInstruments = async () => {
  const result = await API.graphql(graphqlOperation(listInstrumentData));
  console.log(result);
}


export default App;
