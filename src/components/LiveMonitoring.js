import Amplify, { Auth, PubSub } from 'aws-amplify';
import { AWSIoTProvider } from '@aws-amplify/pubsub/lib/Providers';
import { GoogleMap, Marker, useJsApiLoader } from '@react-google-maps/api'
import React, { useEffect, useState } from 'react';
/* global google */

Auth.currentCredentials().then((info) => {
    console.log(info);
});

Amplify.addPluggable(new AWSIoTProvider({
    aws_pubsub_region: 'us-east-2',
    aws_pubsub_endpoint: 'wss://a25w7sbc0kztf-ats.iot.us-east-2.amazonaws.com/mqtt'
}));

function LiveMonitoring() {

    const [markerLocation, setMarkerLocation] = useState({ lat: 40.44015137597415, lng: -80.15832838288605 });

    // Runs only after the first render
    useEffect(() => {
        // Subscribe to the topic
        const subscription = PubSub.subscribe('instrument/live/readings').subscribe({
            next: data => {
                console.log(data);
                setGoogleMarkerPosition(data);
            },
            error: error => console.error('Error receving message', error),
            complete: () => console.log('Done')
        });

        return () => {
            subscription.unsubscribe();
        };
    }, []);

    const setGoogleMarkerPosition = (data) => {
        console.log(data.value.id);
        setMarkerLocation({ lat: parseFloat(data.value.location.latitude), lng: parseFloat(data.value.location.longitude) });
        console.log(markerLocation);
    };

    // const options = {
    //     zoomControlOptions: {
    //         position: google.maps.ControlPosition.RIGHT_CENTER // 'right-center' ,
    //         // ...otherOptions
    //     }
    // };

    const { isLoaded, loadError } = useJsApiLoader({
        googleMapsApiKey: "AIzaSyCYdB4qeWS8MfPu-yZAFBp7J9POBa_xhhY" // ,
        // ...otherOptions
    });

    console.log(loadError);
    console.log(isLoaded);

    const renderMap = () => {
        console.log("rendermap allled");
        // wrapping to a function is useful in case you want to access `window.google`
        // to eg. setup options or create latLng object, it won't be available otherwise
        // feel free to render directly if you don't need that
        // const onLoad = React.useMemo(
        //     function onLoad(mapInstance) {
        //         // do something with map Instance
        //     }, []);

        return <GoogleMap
            id="circle-example"
            mapContainerStyle={{
                height: "600px",
                width: "100%"
            }}
            zoom={19}
            center={{ lat: 40.44015137597415, lng: -80.15832838288605 }}
        >
            <Marker
                icon={{
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 7,
                }}
                position={markerLocation}
            />
        </GoogleMap>
    }

    if (loadError) {
        return <div>Map cannot be loaded right now, sorry.</div>
    }

    return isLoaded ? renderMap() : <h1>Error...</h1>;
    //return <h1>Hello</h1>;
}


export default LiveMonitoring;