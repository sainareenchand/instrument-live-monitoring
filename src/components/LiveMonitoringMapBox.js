import React, { Component } from "react";
import mapboxgl from "mapbox-gl";
import './LiveMonitoring.css';

mapboxgl.accessToken = process.env.REACT_APP_MAPBOX_ACCESS_TOKEN;

export default class LiveMonitoringMapBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            initLocation: {
                lat: 40.44015137597415,
                lng: -80.15832838288605
            },
            zoom: 18,
            markers: [
                {
                    lat: 40.44015137597415,
                    lng: -80.15832838288605
                }
            ]
        }
        this.mapContainer = React.createRef();
    }

    componentDidMount() {
        const { initLocation, zoom } = this.state;
        const map = new mapboxgl.Map({
            container: this.mapContainer.current,
            style: 'mapbox://styles/mapbox/satellite-streets-v11',
            center: [initLocation.lng, initLocation.lat],
            zoom: zoom
        });
    }

    render() {
        return (
            <div>
                <div ref={this.mapContainer} className="map-container">

                </div>
            </div>
        )
    }
}