/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createInstrumentData = /* GraphQL */ `
  mutation CreateInstrumentData($input: CreateInstrumentDataInput!) {
    createInstrumentData(input: $input) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
export const updateInstrumentData = /* GraphQL */ `
  mutation UpdateInstrumentData($input: UpdateInstrumentDataInput!) {
    updateInstrumentData(input: $input) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
export const deleteInstrumentData = /* GraphQL */ `
  mutation DeleteInstrumentData($input: DeleteInstrumentDataInput!) {
    deleteInstrumentData(input: $input) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
