/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getInstrumentData = /* GraphQL */ `
  query GetInstrumentData($id: ID!) {
    getInstrumentData(id: $id) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
export const listInstrumentData = /* GraphQL */ `
  query ListInstrumentData(
    $filter: TableInstrumentDataFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listInstrumentData(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        coReading
        h2SReading
        location
        serialNumber
        recUpdateTime
      }
      nextToken
    }
  }
`;
