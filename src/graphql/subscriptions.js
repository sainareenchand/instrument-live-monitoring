/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateInstrumentData = /* GraphQL */ `
  subscription OnCreateInstrumentData(
    $id: ID
    $coReading: String
    $h2SReading: String
    $location: AWSJSON
    $serialNumber: String
  ) {
    onCreateInstrumentData(
      id: $id
      coReading: $coReading
      h2SReading: $h2SReading
      location: $location
      serialNumber: $serialNumber
    ) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
export const onUpdateInstrumentData = /* GraphQL */ `
  subscription OnUpdateInstrumentData(
    $id: ID
    $coReading: String
    $h2SReading: String
    $location: AWSJSON
    $serialNumber: String
  ) {
    onUpdateInstrumentData(
      id: $id
      coReading: $coReading
      h2SReading: $h2SReading
      location: $location
      serialNumber: $serialNumber
    ) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
export const onDeleteInstrumentData = /* GraphQL */ `
  subscription OnDeleteInstrumentData(
    $id: ID
    $coReading: String
    $h2SReading: String
    $location: AWSJSON
    $serialNumber: String
  ) {
    onDeleteInstrumentData(
      id: $id
      coReading: $coReading
      h2SReading: $h2SReading
      location: $location
      serialNumber: $serialNumber
    ) {
      id
      coReading
      h2SReading
      location
      serialNumber
      recUpdateTime
    }
  }
`;
